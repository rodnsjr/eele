<?php

class Usuario extends Eloquent
{
	protected $table = 'usuario';

	public static $rules = array(
		'username' => 'required',
		'password' => 'required',
		'email' => 'required'
	);
}