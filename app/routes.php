<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index');
});

//Aqui tem que virar controller
Route::get('/aluno', function()
{
	return View::make('ambiente.aluno.index');
});

//Aqui tem que virar controller
Route::get('/professor', function()
{
	return View::make('ambiente.professor.index');
});

Route::resource('tarefa', 'TarefaController');

Route::get('listatarefas', 'TarefaController@show_all');

Route::get('resolvertarefa/{id}', 'TarefaController@show_tarefa');

Route::get('/about', function()
{
	return View::make('about');
});

Route::get('/register', 'UsuarioController@index');

Route::post('/create', 'UsuarioController@create');

Route::get('/t', function()
{
	$data = array('currentPage' => 4, 'usuario' => "t");
	return View::make('usuario.sucess', compact('data'));
});

Route::get('/login', function()
{
	$data = array('currentPage' => 4, 'usuario' => "t");
	return View::make('usuario.login', compact('data'));
});

Route::post('/auth', 'UsuarioController@auth'); 