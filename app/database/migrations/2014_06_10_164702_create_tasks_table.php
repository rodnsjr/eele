<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('texto', function($table)
		{
			$table->increments('id');
			$table->string('frase');
			$table->string('verbos');
			$table->timestamps();
		});

		Schema::create('tarefa', function($table)
		{
			$table->increments('id');
			$table->integer('pontos');
			$table->integer('dificuldade');
			$table->integer('texto_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schemma::drop('tarefa');
		Schema::drop('texto');
	}

}
