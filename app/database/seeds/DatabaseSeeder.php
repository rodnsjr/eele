<?php

class DatabaseSeeder extends Seeder {

    public function run()
    {
        $this->call('UserTableSeeder');

        $this->command->info('Usuario administrador criado!');
    }

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('usuario')->delete();

        Usuario::create(array('username' => 'admin',
        	'password' => 'admin',
        	'permission' => '3',
        	'email' => 'admin@admin.com'));
    }

}