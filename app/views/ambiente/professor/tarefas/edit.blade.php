@extends('ambiente.professor.ambienteprofessor')

<link rel="stylesheet" href="{{URL::asset('/lib/mono-chrome.css')}}"/>
<script src="{{URL::asset('app/voice/texttospeech.js')}}"></script>
<script type="text/javascript" async="" src="{{URL::asset('lib/speech_files/ga.js')}}"></script>
<script src="{{URL::asset('lib/speech_files/autotrack.js')}}"></script>

<style>
	#tarefa_info{
		color:white;
		text-align: center;
		padding: 20px;
	}
</style>

@section('ambiente_content')	
		<article class="first">
			<h2 id="welcome">{{ $tarefa->titulo }}</h2>
				
				<div id="textAudioBox">
				<p>{{$tarefa->texto}}</p>
				
				<div align="right">
				<div class="icon" onclick='playAudio("{{ $tarefa->texto }}")'>
						<div class="play"></div>
					</div>
				</div>
				
				</div>
				<br>
				
				<div id="inputTextBox">
					<h2 id="tarefa_info">Editar Tarefa</h2>
					
					{{ Form::model($tarefa, array('method' => 'PATCH', 'route' => array('tarefa.update', $tarefa->id))) }}
					Titulo da tarefa
					<br>
						{{Form::text('titulo', $tarefa->titulo); }}
					<br>
					<br>
					Texto da tarefa
					<br>
						{{Form::textarea('texto', $tarefa->texto); }}
					<br>
					<br>
				</div>

				<br>
				<ul id="interactionButtons">
				<li>
					{{ Form::submit('Salvar', array('class' => 'button')) }}

					{{Form::close()}}
				</li>
				</ul>

		</article>
@stop