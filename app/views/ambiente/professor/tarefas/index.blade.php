@extends('ambiente.professor.ambienteprofessor')

<link rel="stylesheet" href="{{URL::asset('/lib/mono-chrome.css')}}"/>

@section('ambiente_content')	
			<h2 id="welcome">Tarefas</h2>
			<!-- Table -->
			<div id="sub_menu">
				<a href="#" class="button">
					Apagar									
				</a>
					
				<a href="#" class="button">
					Nova Tarefa
				</a>
				
			</div>
			
			<br>
	@if ($tarefas->count())
			<table id="tarefasList" class="12u">
				<tr id="tarefasListHeader">
					<td>#</td>
					<td>Titulo</td>
					<td>Editar</td>
					<td>Apagar</td>
				</tr>
					@foreach ($tarefas as $tarefa)
						<tr>
							<td><input type="checkbox"></td>
								<td class="10u">{{$tarefa->titulo}}</td>
							<td>
								<div class="icon">
									<div onClick="window.location = '{{ URL::action('TarefaController@edit', $tarefa->id); }}';" class="pencil"></div>
								</div>
							</td>
							<td>
								<div class="icon">
									<div onClick="window.location = 'URL::action('TarefaController@destroy', $tarefa->id);';" class="dustbin"></div>
								</div>											
							</td>
						</tr>
					@endforeach				
				<tr>
			</table>
	@else
	    Não há tarefas
	@endif
				<div id="tarefasListFooter"></div>
@stop