@extends('ambiente.design')
<? $professor = "Professor"; ?>

@section('data')
<? $data = 2; ?>
@stop

@section('userNameText')
	<h3 id="menu_header">{{ $professor }}</h3>
@stop

@section('nav_menu')
	<li><a href="{{ URL::action('TarefaController@index'); }}">Tarefas</a></li>
	<li><a href="#">Alunos</a></li>
@stop