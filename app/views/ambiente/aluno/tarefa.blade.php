@extends('ambiente.aluno.design')
<link rel="stylesheet" href="{{URL::asset('/lib/mono-chrome.css')}}"/>

<script src="{{URL::asset('lib/speech_files/autotrack.js')}}"></script>
<script>
			(function(e, p){
				var m = location.href.match(/platform=(win8|win|mac|linux|cros)/);
				e.id = (m && m[1]) ||
					   (p.indexOf('Windows NT 6.2') > -1 ? 'win8' : p.indexOf('Windows') > -1 ? 'win' : p.indexOf('Mac') > -1 ? 'mac' : p.indexOf('CrOS') > -1 ? 'cros' : 'linux');
				e.className = e.className.replace(/\bno-js\b/,'js');
			  })(document.documentElement, window.navigator.userAgent)
</script>

<script src="{{URL::asset('app/voice/recognition.js')}}"></script>
<script src="{{URL::asset('app/voice/texttospeech.js')}}"></script>
<script type="text/javascript" async="" src="{{URL::asset('lib/speech_files/ga.js')}}"></script>


@section('ambiente_content')
		<article class="first">
			<h2 id="welcome">{{ $tarefa->titulo }}</h2>
				
				<div id="textAudioBox">
				<p>{{$tarefa->texto}}</p>
				
				<div align="right">
				<div class="icon" onclick='playAudio("{{$tarefa->texto}}")'>
						<div class="play"></div>
					</div>
				</div>
				
				</div>
				<br>
				<div id="inputAudioBox">
					<div align="right">	
						<div onclick='initRecognition("inputAudioBoxText")' class="icon">									
							<div class="mic"></div>
							<div class="holder"></div>
						</div>
					</div>
				<p id="inputAudioBoxText">
				_
				</p>
				</div>
				
				<!-- Previsões futuras!!
				<div id="inputTextBox">
					<form>
					Formulário para preencher o texto da tarefa ficará aqui
					<br>
					
					<input type="text" name="inputVerbs" value="verbo">
					</input>
					<input type="text" name="inputVerbs" value="verbo">
					</input>
					<input type="text" name="inputVerbs" value="verbo">
					</input>
					
					<br>
					A quantidade de caixas de texto será dinâmica.
					Ou seja, cada tarefa terá várias partes de texto para serem
					preenchidas.
					</form>
				</div> 
				-->
				<br>
				<ul id="interactionButtons">
				<li>
				<a href="#" class="button">Resolver</a>
				<a href="#" class="button">Submeter</a>
				</li>
				</ul>

		</article>
@stop