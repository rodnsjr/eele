@extends('ambiente.aluno.design')

<link rel="stylesheet" href="{{URL::asset('/lib/mono-chrome.css')}}"/>

@section('ambiente_content')	
			<h2 id="welcome">Tarefas</h2>

	@if ($tarefas->count())
			<table id="tarefasList" class="12u">
				<tr id="tarefasListHeader">
					<td>Titulo</td>
					<td>Resolver</td>
				</tr>
					@foreach ($tarefas as $tarefa)
						<tr>
							<td class="10u">{{ $tarefa->titulo }}</td>
							<td>
								<div onClick="window.location = '{{ action('TarefaController@show_tarefa', $tarefa->id); }}';"  class="icon">
									<div class="briefcase_handle"></div>
									<div class="briefcase"></div>
								</div>
							</td>
						</tr>
					@endforeach				
				<tr>
			</table>
	@else
	    Não há tarefas
	@endif
				<div id="tarefasListFooter"></div>
@stop