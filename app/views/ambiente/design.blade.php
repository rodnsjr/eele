@extends('layout')

@section('main_content')
	<div id="main-wrapper" class="subpage">
					<div class="container">
						<div class="row">
							<div class="3u skel-cell-important" id="menu_bar">
								<!-- Sidebar -->
								<div id="menu_top"></div>
									<section>
										@yield('userNameText')
										<ul class="link-list" id="menu_item">
										@yield('nav_menu')

										</ul>
									</section>
							</div>

							<div class="9u">
							
								<!-- Content -->

									<article class="first">									

									@yield('ambiente_content')

									</article>							

							</div>
						</div>
					</div>
				</div>

@stop