@extends('layout')

@section('data')
<? $data = 0; ?>
@stop

@section('main_content')

			<div id="main-wrapper">
				<div class="container">
					<!-- Banner -->

						<div  class="row">
							<div class="12u">
								<div id="banner">
									<a href="#"><img style="border-radius: 25px 25px 0px 0px ;" src="images/banner.jpg" alt="" /></a>
									<div style="border-radius: 0px 0px 25px 25px;" class="caption">
										<span><strong>English E-Learning Environment</strong>: Aprenda Inglês gratuitamente!</span>
										<a href="{{URL::to('/about')}}" class="button">Veja como!</a>
									</div>
								</div>
							</div>
						</div>
						
					<!-- Thumbnails -->
						
					<!-- CTA Box -->

						<div class="row">
							<div class="12u">
								<div class="cta-box">
									<span>Deseja aprender ou ensinar Inglês?</span>
									<a href="{{URL::to('/register')}}" class="button">Cadastre-se!</a>
								</div>
							</div>
						</div>

				</div>
			</div> 


@stop