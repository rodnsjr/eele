<!DOCTYPE HTML>
<!--
	Arcana 2.1 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>English E-Learning Environment</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700" rel="stylesheet" />
		<script src="{{URL::asset('js/jquery.min.js')}}"></script>
		<script src="{{URL::asset('js/config.js')}}"></script>
		<script src="{{URL::asset('js/skel.min.js')}}"></script>
		<script src="{{URL::asset('js/skel-panels.min.js')}}"></script>

			<link rel="stylesheet" href="{{URL::asset('css/skel-noscript.css')}}" />
			<link rel="stylesheet" href="{{URL::asset('css/style.css')}}" />
			<link rel="stylesheet" href="{{URL::asset('css/style-desktop.css')}}" />
		<!--[if lte IE 9]><link rel="stylesheet" href="{{URL::asset('css/style-ie9.css')}}" /><![endif]-->
		<!--[if lte IE 8]><script src="{{URL::asset('js/html5shiv.js')}}"></script><![endif]-->
	</head>
	<body>
		@yield('data')
		<!-- Header -->
			@yield('header')
			<? 
				//$defaultData;

				//$cvar = $defaultData[0];
				$cvar = 0;
				
				$currentPage = 1;

				if ($data == null)
					$currentPage = $data['currentPage'];
				else
					$currentPage = $data;

				$cvar = $currentPage;

				$cIndex = "";
				$cAluno = "";
				$cProfessor = "";
				$cSobre = "";
				$cRegistro = "";

				if ($cvar == 0)
					$cIndex = "current_page_item";
				else if ($cvar == 1)
					$cAluno =  "current_page_item";
				else if ($cvar == 2)
					$cProfessor = "current_page_item";
				else if ($cvar == 3)
					$cSobre = "current_page_item";
				else if ($cvar == 4)
					$cRegistro = "current_page_item";
			?>

			<div id="header-wrapper">
				<header class="container" id="site-header">
					<div class="row">
						<div class="12u">
							<div id="logo">
								<h1>English E-Learning Environment</h1>
							</div>
							<nav id="nav">
								<ul>
									<li class="{{ $cIndex }}"><a href="{{ URL::to('/')  }}">Início</a></li>
									<li class="{{ $cAluno }}"><a href="{{ URL::to('/aluno')  }}">Alunos</a></li>
									<li class="{{ $cProfessor }}"><a href="{{ URL::to('/professor') }}">Professores</a></li>
									<li class="{{ $cSobre }}"><a href=" {{ URL::to('/about') }}">Sobre</a></li>
									<li class=" {{ $cRegistro }}"><a href="{{ URL::to('/register') }}">Registrar</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</header>
			</div>

		<!-- Main -->
			@yield('main_content')			
		<!-- Footer -->
			@yield('footer')
			<div id="footer-wrapper">
					<div class="row">
						<div class="12u">
							<div id="copyright">
								&copy; English E-Learning Environment. | Design: <a href="http://html5up.net">HTML5 UP</a>
							</div>
						</div>
					</div>
			</div>
	</body>
</html>