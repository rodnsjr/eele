@extends('usuario.design')

@section('main_content')
	<div id="main-wrapper" class="subpage">
		<div class="container">
			<div class="row">
				<div id="registerForm" class="9u skel-cell-important">
					<!-- Content -->
					<section>
						<br>
						<h2>Login</h2>
						{{ Form::open(array('url' => '/auth'))}}
							{{ Form::label('usuario', 'Usuario')}}
							<br>
							{{ Form::text('username')}} 
							<br>
							Email: 
							<br>
							{{ Form::text('email')}}
							<br>
							Senha
							<br>
							{{ Form::password('password')}}
							<br>
							{{ Form::submit('Login', array('class' => 'button'))}}
						{{ Form::close() }}
					</section>
				</div>
			</div>
		</div>
	</div>
@stop