@extends('usuario.design')
<?
	$validerros = $data['erro'];
	$erros = $validerros->messages();
?>

@section('main_content')
		<div id="main-wrapper" class="subpage">
			<div class="container">
				<div class="row">
					<div id="registerForm" class="9u skel-cell-important">
						<!-- Content -->
						<section>
							@foreach ($erros->all() as $erro)
    							{{ $erro }} <br>
							@endforeach
						</section>
					</div>
				</div>
			</div>
		</div>
@stop
