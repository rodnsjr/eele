@extends('usuario.design')

@section('main_content')
	<div id="main-wrapper" class="subpage">
		<div class="container">
			<div class="row">
				<div id="registerForm" class="9u skel-cell-important">
					<!-- Content -->
					<section>
						<br>
						<h2>Cadastro</h2>
						{{ Form::open(array('url' => '/create'))}}
							{{ Form::label('usuario', 'Usuario')}}
							<br>
							{{ Form::text('username')}} 
							<br>
							Email: 
							<br>
							{{ Form::text('email')}}
							<br>
							Senha
							<br>
							{{ Form::password('password')}}
							<br>
							Nome: 
							<br>
							{{ Form::text('firstname')}}
							<br>
							Sobrenome: 
							<br>
							{{ Form::text('lastname')}}
							<br><br>
							{{ Form::submit('Registrar', array('class' => 'button'))}}
						{{ Form::close() }}
					</section>
				</div>
			</div>
		</div>
	</div>
@stop