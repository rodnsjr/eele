@extends('usuario.design')

@section('main_content')
	<div id="main-wrapper" class="subpage">
		<div class="container">
			<div class="row">
				<div id="registerForm" class="9u skel-cell-important">
					<!-- Content -->
						<h1> Bem vindo {{$data['usuario']}}!</h1>
							@if (Auth::check())
								Logado
							@endif
				</div>
			</div>
		</div>
	</div>
@stop