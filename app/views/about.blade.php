@extends('layout')

@section('data')
<? $data = 3; ?>
@stop

@section('main_content')
<div id="main-wrapper" class="subpage">
	<div class="container">
		<div class="row">
			<div class="12u skel-cell-important">

<!-- Content -->

<article class="first last">

<h2>Sobre o ambiente de aprendizado de Inglês</h2>

<p>Este ambiente permite professores e alunos compartilhar de tarefas para o ensino do Inglês.
Os professores criam tarefas e atrelam estas aos alunos que por sua vez resolvem estas. Após completas
as tarefas, os alunos submetem-nas aos professores, para a avaliação de nota.</p>

<p>Este ambiente conta com recursos de reconhecimento de voz, garantindo que os alunos possam
praticar a sua pronuncia. Além disso este ambiente também conta com recursos de texto para fala,
garantindo que professores criem dialogos dinâmicos para os alunos interagirem.</p>

<p>Para começar a utilizar o sistema, basta <a class="common-link" href="{{ URL::to('/register') }}">cadastrar-se</a>.</p>

</article>	

			</div>
		</div>
	</div>
</div>
@stop
