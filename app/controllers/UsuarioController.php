<?php

class UsuarioController extends BaseController {

	public function index()
	{
		return View::make('usuario.register');
	}

	public function auth()
	{
		$input = Input::all();

		$validation = Validator::make($input, Usuario::$rules);

		if (!$validation->passes())
		{
			$data = array('erro' => $validation);
			return View::make('usuario.erro',compact('data'));
		}

		$usuario = array(
			'username' => $input['username'],
			'email' => $input['email'],
			'password' => $input['password']
			);

		if (Auth::attempt($usuario))
		{
			/*
			$data = array('currentPage' => 4, 'usuario' => $nomeUsuario);
			return View::make('usuario.sucess', compact('data'));*/
		}
		else
		{
			$data = array('currentPage' => 4, 'erro' => $validation);
			return View::make('usuario.erro',compact('data'));
		}
	}

	public function create()
	{
		$input = Input::all();
		//validar
		$validation = Validator::make($input, Usuario::$rules);

		if (!$validation->passes())
		{
			$data = array('currentPage' => 4, 'erro' => $validation);
			return View::make('usuario.erro',compact('data'));
		}

		$novoUsuario = new Usuario;
		$novoUsuario->username = $input['username'];
		$password = $input['password'];
		$password = Hash::make('secret');
		$novoUsuario->password = $password;
		$novoUsuario->email = $input['email'];
		$novoUsuario->firstname = $input['firstname'];
		$novoUsuario->lastname = $input['lastname'];
		$novoUsuario->permission = 1;
		$novoUsuario->save();

		$nomeUsuario = $input['username']; 
		$data = array('currentPage' => 4, 'usuario' => $nomeUsuario);
		return View::make('usuario.sucess', compact('data'));
	}

}