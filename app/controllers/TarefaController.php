<?php

class TarefaController extends BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /tarefa
	 *
	 * @return Response
	 */
	protected $tarefa;
	public function __construct(Tarefa $tarefa)
	{
		$this->tarefa = $tarefa;
	}

	public function index()
	{
		$tarefas = $this->tarefa->all();
		return View::make('ambiente.professor.tarefas.index', compact('tarefas'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /tarefa/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /tarefa
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /tarefa/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$tarefa = $this->tarefa->find($id);
		return View::make('ambiente.professor.tarefas.show', compact('tarefa'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /tarefa/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$tarefa = $this->tarefa->find($id);
		return View::make('ambiente.professor.tarefas.edit', compact('tarefa'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /tarefa/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$tarefa = $this->tarefa->find($id);
		$input = Input::all();

		$tarefa->titulo = $input['titulo'];
		$tarefa->texto = $input['texto'];
		
		if ($tarefa->save())
		{
			return View::make('ambiente.professor.tarefas.show', compact('tarefa'));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /tarefa/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$tarefa = $this->tarefa->find($id)->delete();

		return View::make('ambiente.professor.tarefas.index', compact('tarefas'));
	}

	public function show_all()
	{
		//
		$tarefas = $this->tarefa->all();
		return View::make('ambiente.aluno.tarefas', compact('tarefas'));
	}

	public function show_tarefa($id)
	{
		$tarefa = $this->tarefa->find($id);
		return View::make('ambiente.aluno.tarefa', compact('tarefa'));
	}

}