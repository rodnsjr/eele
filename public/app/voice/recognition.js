function initRecognition(value){
	console.log(value);
	
	document.getElementById(value).innerHTML = "listening ...";
	
	var recognition = new webkitSpeechRecognition();
	recognition.continuous = false;
	recognition.interimResults = true;
	recognition.lang = "en";
	recognition.start();
	
	recognition.onresult = function(event) { 
	    var interim_transcript = '';
		
		if (typeof(event.results) == 'undefined') {
		  recognition.onend = null;
		  recognition.stop();
		  upgrade();
		  return;
		}
		
		for (var i = event.resultIndex; i < event.results.length; ++i) {
		  if (event.results[i].isFinal) {
			interim_transcript += event.results[i][0].transcript;
		  } else {
			interim_transcript += event.results[i][0].transcript;
		  }
		}
		
		document.getElementById(value).innerHTML = interim_transcript;
		
		};
	  
}